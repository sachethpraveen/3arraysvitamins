const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

// Problem 1

function availableItems(array) {
  return array.filter((item) => {
    return item.available === true;
  });
}

// Problem 2
function containOnlyVitamin(array, vitamin) {
  return array.filter((item) => {
    return item.contains.split(",").length === 1 && item.contains === vitamin;
  });
}

// Problem 3
function containVitamin(array, vitamin) {
  return array.filter((item) => {
    return item.contains.includes(vitamin);
  });
}

// Problem 4
function groupItems(array) {
  return array.reduce((accumulator, current) => {
    let vitaminArray = current.contains.split(", ");
    vitaminArray.forEach((item) => {
      if (accumulator.hasOwnProperty(item)) {
        accumulator[item].push(current.name);
      } else {
        accumulator[item] = [current.name];
      }
    });
    return accumulator;
  }, {});
}

console.log(groupItems(items));

// Problem 5
function sortItems(array) {
  return array.sort((currentItem, nextItem) => {
    return nextItem["contains"].length - currentItem["contains"].length;
  });
}
